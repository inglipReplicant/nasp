extern crate avl;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::stdin;
use avl::{AvlTreeSet};

fn main() {
    let args: Vec<String> = env::args().collect();
    println!("Opening file {:?}", args[1]);

    let mut file = File::open(&args[1]).expect("Error while opening file, check path");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Unable to read from file");

    let mut set = AvlTreeSet::new();
    for num in contents.trim().split(' ') {
        set.insert(num.trim().parse::<i32>().unwrap());
    }

    for e in set.iter() {
        print!("{} ", e);
    }
    println!();

    loop {
        let mut num = String::new();
        stdin().read_line(&mut num).expect("Failed to read number from CLI");
        set.insert(num.trim().parse::<i32>().unwrap());
        for e in set.iter() {
            print!("{} ", e);
        }
        println!();
    }
}
